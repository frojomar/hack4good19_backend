#!/bin/bash

while read p; do
    if [[ "$p" == "#"* ]];then
        echo "skip comment line"
    else
        if [[ "$p" == "HOST="* ]];then
            host="$(cut -d'=' -f2 <<<"$p")"
            echo "Linea de host leida: $host"
        fi
        if [[ "$p" == "PORT="* ]];then
            port="$(cut -d'=' -f2 <<<"$p")"
            echo "Linea de puerto leida: $port"
        fi
        echo "$p"
    fi
done <settings.conf

export FLASK_APP=./src/main.py
source venv/bin/activate
flask run -h $host -p $port
