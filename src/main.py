# coding=utf-8

from flask import Flask, jsonify, request, send_file, Response
from flask_restful import Api
from flask_cors import CORS

from werkzeug.utils import secure_filename
import os

from src.readConfigurationsFile import loadConfigurations
from src.entities.base import Session, engine, Base

from .opendata.loaddata import loadDataCenters, loadDataLocalidades

from src.resources.resources import *
from src.entities.user import User, UserSchema
from src.entities.ad import Ad, AdSchema
from src.entities.center import Center, CenterSchema
from src.entities.localidad import Localidad, LocalidadSchema
from src.entities.travel import Travel, TravelSchema




UPLOAD_FOLDER = 'src/perfilphotos'
ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg'])

# creating the Flask application
app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
#cors = CORS(app, resources={r"/*": {"origins": "*"}})
CORS(app)
api = Api(app)

# if needed, generate database schema
Base.metadata.create_all(engine)

configurations = {}
loadConfigurations('settings.conf', configurations)

loadDataCenters()
loadDataLocalidades()

api.add_resource(RegistryResource, '/registry')
api.add_resource(LoginResource, '/login')

api.add_resource(UserResource, '/user')
api.add_resource(UserInfoResource, '/user/<int:id>') #devuelve la información del usuario cuyo ID coincida con el indicado (GET)
# api.add_resource(PhotoResource, '/user/photo')

api.add_resource(CenterResource, '/center') #Recuperar los centros que existen
api.add_resource(LocalidadResource, '/localidad') #Recuperar las localidades que existen

api.add_resource(AdResource, '/ad') #permite añadir un anuncio y recuperar la información de mis anuncios
api.add_resource(AdWithIdResource, '/ad/<int:id>') #permite recuperar la información de un anuncio en el que he sido o seré pasajero (GET) y finalizarlo (PUT)

api.add_resource(AdRecommendedResource, '/ad/recommended') #Devuelve los anuncios para un día y hora próximos ( 40 min de diferencia) (en el mismo centro u otro de la localidad)
api.add_resource(AdDayResource, '/ad/allday') #Devuelve los anuncios para ese día y ese centro

api.add_resource(TravelResource, '/ad/<id>/travel') #Permite solicitar viajar en un anuncio(POST), ver el estado de una solicitud o viaje confirmado(GET) y eliminar una solicitud(DELETE)
api.add_resource(TravelConfirmResource, '/ad/<id_ad>/travel/<id_user>/confirm') #Permite al anunciante obtener la información de una solicitud (GET) y confirmar o denegarla(PUT)
#     poner a revisado, sea el resultado que sea
api.add_resource(AllTravelResource, '/ad/travel') #Permite recuperar la información de los viajes que tiene solicitados o aceptados

api.add_resource(MyOwnTravelsSolicitedResource, '/ad/<id>/solicited') #Permite recuperar la información de las solicitudes pendientes de aceptar o denegar que van en un viaje propio
api.add_resource(MyOwnTravelsTravellersResource, '/ad/<id>/travellers') #Permite recuperar la información de los viajeros confirmados que van en un viaje propio
api.add_resource(MyOwnTravelsResource, '/ad/<id>/travellers/<id_user>') #Permite eliminar un viaje confirmado, al creador del viaje, para el usuario indicado

@app.route('/user/photo', methods=['POST'])
def upload_photo():
    username = request.headers.get('Authorization', default="")
    if username == "":
        return jsonify({"error": "debe especificarse el usuario logueado"}), 400

    data = request.get_json()
    session = Session()
    user = session.query(User).filter_by(username=username).first()

    # check if the post request has the file part
    if 'file' not in request.files:
        user.photo = ""
    else:
        file = request.files['file']
        if file.filename == '':
            user.photo = ""
        else:
            filename = secure_filename(file.filename)
            print(os.path.join(""))
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            user.photo = filename

    photoname=user.photo
    session.commit()
    session.close()
    return jsonify({"status": "OK", "photoname": str(photoname)}), 200


@app.route('/user/photo/<photoname>', methods=['GET'])
@app.route('/user/photo/', defaults={'photoname': ""})
def get_photo(photoname):
    # username = request.headers.get('Authorization', default="")
    # if username == "":
    #     return jsonify({"error": "debe especificarse el usuario logueado"}), 400
    if str(photoname)== "":
        photoname="default.jpg"
    abs_path = (os.path.join(app.config['UPLOAD_FOLDER'], photoname))
    print(abs_path)
    if not os.path.exists(abs_path):
        response = Response()
        response.status_code=204
        return response
    else:
        if os.path.isfile(abs_path):
            folder=app.config['UPLOAD_FOLDER'].replace("src/", "")
            print(folder)
            rel_path = (os.path.join(folder, photoname))
            return send_file(rel_path)
        response = Response()
        response.status_code=404
        return response