# coding=utf-8

from sqlalchemy import Column, Integer, String, Float, PrimaryKeyConstraint, ForeignKeyConstraint, TIME, Boolean

from .base import Base

from marshmallow import Schema, fields


class Travel(Base):

    __tablename__ = 'Travel'

    id_user = Column(Integer, nullable=False, unique= False)
    id_ad = Column(Integer, nullable=False, unique= False)
    hora_cita = Column(TIME, nullable=False, unique= False)
    valoration = Column(Integer, nullable=False, unique= False, default=-1)
    creator = Column(Boolean, nullable=False, unique= False, default= False)
    acepted = Column(Boolean, nullable=False, unique= False, default= False)
    revised = Column(Boolean, nullable=False, unique= False, default= False)

    __table_args__ = (
        PrimaryKeyConstraint('id_user', 'id_ad'),
        ForeignKeyConstraint(['id_user'], ['User.id']),
        ForeignKeyConstraint(['id_ad'], ['Ad.id']),
        {},
    )

    def __init__(self, id_user, id_ad, hora_cita, creator):
        self.id_user = id_user
        self.id_ad = id_ad
        self.hora_cita = hora_cita
        self.creator = creator


class TravelSchema(Schema):
    id_user = fields.Number()
    id_ad = fields.Number()
    hora_cita = fields.Time()
    valoration = fields.Number()
    creator = fields.Boolean()
    acepted = fields.Boolean()
    revised = fields.Boolean()


