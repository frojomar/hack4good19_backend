# coding=utf-8

from sqlalchemy import Column, Integer, String, Float

from .base import Base

from marshmallow import Schema, fields


class User(Base):

    __tablename__ = 'User'

    id = Column(Integer, primary_key=True, autoincrement=True)
    username = Column(String(100), nullable=False, unique= True)
    password = Column(String(100), nullable=False, unique= True)
    firstname = Column(String(100), nullable=False, unique= False)
    lastname = Column(String(100), nullable=False, unique= False)
    years = Column(Integer, nullable=False, unique= False)
    genre = Column(String(1), nullable=False, unique= False)
    photo = Column(String(150), nullable=False, unique= False)
    telephone = Column(String(50), nullable=False, unique= False)
    avg_judgment = Column(Float(2,1), nullable=False, unique= False)
    number_judgment = Column(Integer, nullable=False, unique= False)


    # __table_args__ = (
    #     PrimaryKeyConstraint('id'),
    #     {},
    # )

    def __init__(self, username, password, firstname, lastname, years, genre, telephone, photo):
        self.username = username
        self.password = password
        self.firstname = firstname
        self.lastname = lastname
        self.years = years
        self.genre = genre
        self.photo = photo
        self.telephone = telephone
        self.avg_judgment = 0.0
        self.number_judgment = 0


class UserSchema(Schema):
    id = fields.Integer()
    username = fields.Str()
    firstname = fields.Str()
    lastname = fields.Str()
    years = fields.Integer()
    genre = fields.Str()
    photo = fields.Str()
    avg_judgment = fields.Float()
    number_judgment = fields.Integer()
    telephone = fields.Str()

