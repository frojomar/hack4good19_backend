# coding=utf-8

from sqlalchemy import Column, Integer, String, Float, Boolean

from .base import Base

from marshmallow import Schema, fields


class Center(Base):

    __tablename__ = 'Center'

    id = Column(Integer, primary_key=True, autoincrement=True)
    nombre = Column(String(150), nullable=False, unique= True)
    latitud = Column(Float, nullable=False, unique= False)
    longitud = Column(Float, nullable=False, unique= False)
    active = Column(Boolean, nullable=False, unique= False)
    id_nexo = Column(Integer, nullable=False, unique= False)

    # __table_args__ = (
    #     PrimaryKeyConstraint('id'),
    #     {},
    # )

    def __init__(self, nombre, latitud, longitud, active, id_nexo):
        self.nombre = nombre
        self.latitud = latitud
        self.longitud = longitud
        self.active = active
        self.id_nexo = id_nexo


class CenterSchema(Schema):
    id = fields.Number()
    nombre = fields.Str()
    latitud = fields.Float()
    longitud = fields.Float()
    active = fields.Boolean()

