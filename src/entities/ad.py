# coding=utf-8

from sqlalchemy import Column, Integer, String, Float, PrimaryKeyConstraint, ForeignKeyConstraint, TIME, Boolean, DATE

from .base import Base

from marshmallow import Schema, fields


class Ad(Base):

    __tablename__ = 'Ad'

    id = Column(Integer, primary_key=True, nullable=False, unique= True)
    id_creator = Column(Integer, nullable=False, unique=False)
    id_localidad = Column(Integer, nullable=False, unique=False)
    id_center = Column(Integer, nullable=False, unique=False)
    day = Column(DATE, nullable=False, unique=False)
    time_go = Column(TIME, nullable=False, unique=False)
    time_arrive_provisional = Column(TIME, nullable=False, unique=False)
    time_last_appointment = Column(TIME, nullable=False, unique=False)
    max_persons = Column(Integer, nullable=False, unique=False)
    confirmed_persons = Column(Integer, nullable=False, unique=False)
    solicited_persons = Column(Integer, nullable=False, unique=False)
    finalized = Column(Boolean, nullable=False, unique=False, default=False)

    __table_args__ = (
        ForeignKeyConstraint(['id_creator'], ['User.id']),
        ForeignKeyConstraint(['id_localidad'], ['Localidad.id']),
        ForeignKeyConstraint(['id_center'], ['Center.id']),
        {},
    )

    def __init__(self, id_creator, id_localidad, id_center, day, time_go, time_arrive_provisional,
                 max_persons):
        self.id_creator = id_creator
        self.id_localidad = id_localidad
        self.id_center = id_center
        self.day = day
        self.time_go = time_go
        self.time_arrive_provisional = time_arrive_provisional
        self.time_last_appointment = time_arrive_provisional
        self.max_persons = max_persons
        self.confirmed_persons = 0
        self.solicited_persons = 0
        self.finalized = False


class AdSchema(Schema):
    id = fields.Number()
    id_creator = fields.Number()
    id_localidad = fields.Number()
    id_center = fields.Number()
    day = fields.Date()
    time_go = fields.Time()
    time_arrive_provisional = fields.Time()
    time_last_appointment = fields.Time()
    max_persons = fields.Number()
    confirmed_persons = fields.Number()
    solicited_persons = fields.Integer()
    finalized = fields.Boolean()


