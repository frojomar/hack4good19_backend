# coding=utf-8

from sqlalchemy import Column, Integer, String, Float

from .base import Base

from marshmallow import Schema, fields

class Localidad(Base):

    __tablename__ = 'Localidad'

    id = Column(Integer, primary_key=True, autoincrement=True)
    nombre = Column(String(150), nullable=False, unique= True)
    latitud = Column(Float, nullable=False, unique= False)
    longitud = Column(Float, nullable=False, unique= False)


    # __table_args__ = (
    #     PrimaryKeyConstraint('id'),
    #     {},
    # )

    def __init__(self, nombre, latitud, longitud):
        self.nombre = nombre
        self.latitud = latitud
        self.longitud = longitud


class LocalidadSchema(Schema):
    id = fields.Number()
    nombre = fields.Str()
    latitud = fields.Float()
    longitud = fields.Float()

