# coding=utf-8

from datetime import datetime
from sqlalchemy import create_engine, Column, String, Integer, DateTime
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from ..readConfigurationsFile import loadConfigurations

configurations = {}
loadConfigurations('settings.conf', configurations)
print(configurations)

db_host = configurations['HOST_MARIA']
db_port = configurations['PORT_MARIA']
db_name = configurations['DATABASE_MARIA']
db_user = configurations['USER_MARIA']
db_password = configurations['PASS_MARIA']

engine = create_engine('mysql+pymysql://'+str(db_user)+':'+str(db_password)+'@'+str(db_host)+':'+str(db_port)+'/'+str(db_name), echo=False)
#engine = create_engine(f'postgresql://{db_user}:{db_password}@{db_url}/{db_name}')
#engine = create_engine('sqlite:///db.sqlite3')
Session = sessionmaker(bind=engine)

Base = declarative_base()


