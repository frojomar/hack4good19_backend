from flask_restful import Resource
from flask import request, jsonify, Response


from src.entities.base import Session, engine, Base
from src.entities.user import User, UserSchema
from src.entities.ad import Ad, AdSchema
from src.entities.center import Center, CenterSchema
from src.entities.localidad import Localidad, LocalidadSchema
from src.entities.travel import Travel, TravelSchema

from .utils import addSecs, restSecs, formatTime, distKmMenor

class LoginResource(Resource):
    #loguearse en la aplicación
    def post(self):
        data = request.get_json()
        session = Session()
        user = session.query(User).filter_by(username=data['username']).first()
        session.close()

        if not user:
            return {'success': False,'message': 'Identificación fallida'}, 200

        #TODO Añadir el tratamiento con HASH
        #if Administrator.verify_hash(data['password'], administrator.password):
        if data['password'] == user.password :
            return {
                'success': True,
                'message': 'Logueado como {}'.format(user.username)
            }, 200
        else:
            return {'success': False, 'message': 'Identificación fallida'}, 200


class RegistryResource(Resource):
    #registrarse en la aplicación
    def post(self):
        data = request.get_json()
        session = Session()
        user = session.query(User).filter_by(username=data['username']).first()

        if user:
            session.close()
            return {'success': False,'message': 'El nombre de usuario no es válido'}, 400
        else:
            posted_user = UserSchema(only=('username', 'password', 'firstname', 'lastname', 'years', 'genre', 'telephone')).load(request.get_json())
            new_user = User(**posted_user.data, photo="")
            print(user)
            session.add(new_user)
            session.commit()
            new_user_data = UserSchema().dump(new_user).data
            session.close()
            print(new_user_data)
            response = jsonify(new_user_data)
            response.status_code = 201
            return response


class UserResource(Resource):
    #editar información
    def post(self):
        username= request.headers.get('Authorization', default="")
        if username == "":
            return jsonify({"error":"debe especificarse el usuario logueado"}), 400

        data = request.get_json()
        session = Session()
        user = session.query(User).filter_by(username=username).first()

        user.lastname = data['lastname']
        user.firstname = data['firstname']
        user.genre = data['genre']
        user.years = data['years']
        user.telephone = data['telephone']

        session.commit()
        session.close()

        response = Response()
        response.status_code = 200
        return response

    #obtener información para el perfil del usuario
    def get(self):
        username = request.headers.get('Authorization', default="")
        if username == "":
            return jsonify({"error": "debe especificarse el usuario logueado"}), 400
        print("Solicitando información de "+str(username))
        session = Session()
        user = session.query(User).filter_by(username=username).first()
        session.close()
        if user:
            schema = UserSchema(many=False)
            user_data=schema.dump(user).data
            response = jsonify(user_data)
            response.status_code = 200
            return response
        else:
            response = Response()
            response.status_code = 204
            return response


class UserInfoResource(Resource):
    def get(self, id):
        session = Session()
        user = session.query(User).filter_by(id=id).first()
        session.close()
        if user:
            schema = UserSchema(many=False)
            user_data = schema.dump(user).data
            response = jsonify(user_data)
            response.status_code = 200
            return response
        else:
            response = Response()
            response.status_code = 204
            return response


# class PhotoResource(Resource):
#     #editar foto de usuario
#     def post(self):
#
#         username = request.headers.get('Authorization', default="")
#         if username == "":
#             return jsonify({"error": "debe especificarse el usuario logueado"}), 400
#
#         data = request.get_json()
#         session = Session()
#         user = session.query(User).filter_by(username=username).first()
#
#         # check if the post request has the file part
#         if 'file' not in request.files:
#             user.photo=""
#         else:
#             file = request.files['file']
#             if file.filename == '':
#                 user.photo = ""
#             else:
#                 filename = secure_filename(file.filename)
#                 file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
#                 user.photo=filename
#
#         session.commit()
#         session.close()
#         return {"status": "OK", "photoname": str(user.photo)}, 200


class CenterResource(Resource):
    #recuperar los centros que existen
    def get(self):
        session = Session()
        centers_objects = session.query(Center).all()
        centers = CenterSchema(many=True).dump(centers_objects).data
        session.close()
        if len(centers) == 0:
            response = jsonify(centers)
            response.status_code = 204
            return response
        else:
            response = jsonify(centers)
            response.status_code = 200
            return response

class LocalidadResource(Resource):
    #recuperar las localidades que existen
    def get(self):
        session = Session()
        localidad_objects = session.query(Localidad).all()
        localidades = LocalidadSchema(many=True).dump(localidad_objects).data
        session.close()
        if len(localidades) == 0:
            response = jsonify(localidades)
            response.status_code = 204
            return response
        else:
            response = jsonify(localidades)
            response.status_code = 200
            return response


class AdResource(Resource):
    #recuperar mis anuncios
    def get(self):
        username = request.headers.get('Authorization', default="")
        if username == "":
            return jsonify({"error": "debe especificarse el usuario logueado"}), 400
        print("Solicitando anuncios de "+str(username))
        session = Session()
        user = session.query(User).filter_by(username=username).first()
        ad_objects = session.query(Ad).filter_by(id_creator=user.id).order_by(Ad.day.desc(), Ad.time_go.desc()).all()
        session.close()
        if ad_objects:
            schema = AdSchema(many=True)
            ad_data=schema.dump(ad_objects).data
            response = jsonify(ad_data)
            response.status_code = 200
            return response
        else:
            response = Response()
            response.status_code = 204
            return response

    #añadir un nuevo anuncio
    def post(self):
        username = request.headers.get('Authorization', default="")
        if username == "":
            return jsonify({"error": "debe especificarse el usuario logueado"}), 400
        print("Solicitando anuncios de "+str(username))
        session = Session()
        user = session.query(User).filter_by(username=username).first()
        posted_ad = AdSchema(
            only=('id_localidad', 'id_center', 'day', 'time_go', 'time_arrive_provisional',
                  'max_persons')).load(request.get_json())
        posted_ad.data['id_creator']=user.id
        print(posted_ad)
        new_ad = Ad(**posted_ad.data)
        session.add(new_ad)
        session.commit()
        new_travel = Travel(user.id,new_ad.id,new_ad.time_arrive_provisional,True)
        session.add(new_travel)
        session.commit()
        new_ad_data = AdSchema().dump(new_ad).data
        print(new_ad_data)
        response = jsonify(new_ad_data)
        session.close()
        response.status_code = 201
        return response


class AdWithIdResource(Resource):
    #recuperar la informacion de un anuncio (mío o de otra persona)
    def get(self, id):
        print("Solicitando anuncio "+str(id))
        session = Session()
        ad_object = session.query(Ad).filter_by(id=id).first()
        session.close()
        if ad_object:
            schema = AdSchema(many=False)
            ad_data=schema.dump(ad_object).data
            response = jsonify(ad_data)
            response.status_code = 200
            return response
        else:
            response = Response()
            response.status_code = 204
            return response

    #finalizar un viaje
    def put(self, id):
        username = request.headers.get('Authorization', default="")
        if username == "":
            return jsonify({"error": "debe especificarse el usuario logueado"}), 400
        print("Solicitando anuncios de "+str(username))
        session = Session()
        user = session.query(User).filter_by(username=username).first()
        ad_object = session.query(Ad).filter_by(id_creator=user.id, id=id).first()
        data= request.get_json()
        if ad_object:
            ad_object.finalized=data['finalized']
            if ad_object.finalized:
                travels = session.query(Travel).filter_by(id_ad=ad_object.id).all()
                for travel in travels:
                    travel.revised=True
            session.commit()
            ad_data = AdSchema().dump(ad_object).data
            session.close()
            response = jsonify(ad_data)
            response.status_code = 201
            return response
        else:
            response = Response()
            response.status_code = 400
            return response

class TravelResource(Resource):
    #solicitar viajar en un anuncio(POST)
    def post(self, id):
        username = request.headers.get('Authorization', default="")
        if username == "":
            return jsonify({"error": "debe especificarse el usuario logueado"}), 400
        print("Solicitando viaje de "+str(username))
        session = Session()
        user = session.query(User).filter_by(username=username).first()
        posted_travel = TravelSchema(only=('hora_cita')).load(request.get_json())
        data = request.get_json()
        print(data)
        print(posted_travel.data)
        posted_travel.data['hora_cita']=data
        posted_travel.data['id_user']=user.id
        posted_travel.data['creator']=False
        posted_travel.data['id_ad']=id
        print(posted_travel)
        new_travel = Travel(**posted_travel.data)
        ad= session.query(Ad).filter_by(id=new_travel.id_ad).first()
        ad.solicited_persons= ad.solicited_persons+1
        session.add(new_travel)
        session.commit()
        new_travel_data = TravelSchema().dump(new_travel).data
        print(new_travel_data)
        response = jsonify(new_travel_data)
        session.close()
        response.status_code = 201
        return response

    # ver el estado de una solicitud o viaje confirmado(GET)
    def get(self, id):
        username = request.headers.get('Authorization', default="")
        if username == "":
            return jsonify({"error": "debe especificarse el usuario logueado"}), 400
        print("Solicitando viaje de " + str(username))
        session = Session()
        user = session.query(User).filter_by(username=username).first()
        travel = session.query(Travel).filter_by(id_user=user.id, id_ad=id).first()
        if not travel:
            session.close()
            response = Response()
            response.status_code = 204
            return response
        travel_data = TravelSchema().dump(travel).data
        session.close()
        response = jsonify(travel_data)
        response.status_code = 200
        return response


    # eliminar una solicitud(DELETE)
    def delete(self,id):
        username = request.headers.get('Authorization', default="")
        if username == "":
            return jsonify({"error": "debe especificarse el usuario logueado"}), 400
        print("Solicitando viaje de "+str(username))
        session = Session()
        user = session.query(User).filter_by(username=username).first()
        travel = session.query(Travel).filter_by(id_user=user.id, id_ad=id).first()
        if not travel:
            response = jsonify({"error": "no se encuentra el viaje para el usuario"})
            response.status_code = 400
            return response
        if travel.acepted == True:
            response = jsonify({"error": "El viaje ha sido aceptado y solo puede eliminarse por el creador del anuncio"})
            response.status_code = 400
            return response
        session.delete(travel)
        ad = session.query(Ad).filter_by(id=id).first()
        ad.solicited_persons = ad.solicited_persons - 1
        session.commit()
        session.close()
        response = Response()
        response.status_code = 204
        return response


class TravelConfirmResource(Resource):
    def get(self, id_ad, id_user):
        username = request.headers.get('Authorization', default="")
        if username == "":
            return jsonify({"error": "debe especificarse el usuario logueado"}), 400
        print("Solicitando viaje de " + str(username))
        session = Session()
        user = session.query(User).filter_by(username=username).first()
        ad = session.query(Ad).filter_by(id_creator=user.id, id=id_ad).first()
        if not ad:
            session.close()
            return jsonify({"error": "No eres propietario de ese anuncio"}), 400
        travel = session.query(Travel).filter_by(id_user=id_user, id_ad=id_ad, creator=False).first()
        if not travel:
            session.close()
            response = Response()
            response.status_code = 204
            return response
        travel_data= TravelSchema().dump(travel).data
        session.close()
        response = jsonify(travel_data)
        response.status_code = 200
        return response


    def put(self, id_ad, id_user):
        username = request.headers.get('Authorization', default="")
        if username == "":
            return jsonify({"error": "debe especificarse el usuario logueado"}), 400
        print("Solicitando viaje de " + str(username))
        session = Session()
        user = session.query(User).filter_by(username=username).first()
        ad = session.query(Ad).filter_by(id_creator=user.id, id=id_ad).first()
        if not ad:
            session.close()
            return jsonify({"error": "No eres propietario de ese anuncio"}), 400
        travel = session.query(Travel).filter_by(id_user=id_user, id_ad=id_ad, creator=False).first()
        if not travel:
            session.close()
            response = Response()
            response.status_code = 204
            return response
        data = request.get_json()
        travel.acepted= data['acepted']
        travel.revised= True
        if travel.acepted:
            ad.confirmed_persons = ad.confirmed_persons + 1
            if travel.hora_cita < ad.time_arrive_provisional:
                ad.time_arrive_provisional = travel.hora_cita
            elif travel.hora_cita > ad.time_last_appointment:
                ad.time_last_appointment = travel.hora_cita
        ad.solicited_persons = ad.solicited_persons - 1
        session.commit()
        travel_data= TravelSchema().dump(travel).data
        session.close()
        response = jsonify(travel_data)
        response.status_code = 200
        return response


class AllTravelResource(Resource):
    #Permite recuperar la información de los viajes que tiene solicitados o aceptados
    def get(self):
        username = request.headers.get('Authorization', default="")
        if username == "":
            return jsonify({"error": "debe especificarse el usuario logueado"}), 400
        print("Solicitando viaje de " + str(username))
        session = Session()
        user = session.query(User).filter_by(username=username).first()
        travels = session.query(Travel).filter_by(id_user=user.id, creator=False).all()
        session.close()
        if not travels:
            response = Response()
            response.status_code = 204
            return response
        travels_data = TravelSchema(many=True).dump(travels).data
        response = jsonify(travels_data)
        response.status_code = 200
        return response


def removeLocalidades(lat1, long1, ads):
    session = Session()
    ads_final=[]
    for ad in ads:
        localidad = session.query(Localidad).filter_by(id=ad['id_localidad']).first()
        print("Hola")
        if distKmMenor(lat1=lat1,long1=long1, lat2=localidad.latitud, long2=localidad.longitud, km=20):
            ads_final.append(ad)
    return ads_final


class AdRecommendedResource (Resource):
    #Devuelve los anuncios para un día y hora próximos ( 1h de diferencia) (en el mismo centro u otro de la localidad)
    #Lugar de partida menor de 20km
    # day, hour_appointment, id_center
    def get(self):
        username = request.headers.get('Authorization', default="")
        if username == "":
            return jsonify({"error": "debe especificarse el usuario logueado"}), 400
        print("Buscando viajes recomendados para" + str(username))
        day = request.args.get('day')
        hour_appointment = request.args.get('hour_appointment')
        id_center = request.args.get('id_center')
        id_localidad = request.args.get('id_localidad')


        session = Session()
        user = session.query(User).filter_by(username=username).first()
        center = session.query(Center).filter_by(id=id_center).first()
        id_nexo = center.id_nexo
        other_centers = session.query(Center).filter_by(id_nexo=id_nexo).filter(Center.id != id_center).all()

        other_centers_ids = []
        for o_cen in other_centers:
            other_centers_ids.append(o_cen.id)

        time = formatTime(hour_appointment)
        time_init=restSecs(time,3600)
        time_fin=addSecs(time,3600)

        l = session.query(Localidad).filter_by(id=id_localidad).first()

        ads_final=[]
        ads= session.query(Ad).filter_by(id_center=center.id).filter(Ad.id_creator != user.id)\
            .filter(Ad.day.between(day, day)).filter(Ad.time_arrive_provisional >= time_init)\
            .filter(Ad.time_last_appointment <= time_fin).all()

        ads_one = AdSchema(many=True).dump(ads).data
        ads_final.extend(ads_one)
        ads_other = session.query(Ad).filter(Ad.id_center.in_(other_centers_ids)).filter(Ad.id_creator != user.id) \
            .filter(Ad.day.between(day, day)).filter(Ad.time_arrive_provisional >= time_init)\
            .filter(Ad.time_last_appointment <= time_fin).all()
        ads_final.extend(AdSchema(many=True).dump(ads_other).data)

        print(ads_final)
        ads_final2 = removeLocalidades(l.latitud, l.longitud, ads_final)

        response = jsonify(ads_final2)
        response.status_code = 200
        return response




class AdDayResource (Resource):
    #Devuelve los anuncios para ese día (en el mismo centro u otro de la localidad)
    # day, id_center
    def get(self):
        username = request.headers.get('Authorization', default="")
        if username == "":
            return jsonify({"error": "debe especificarse el usuario logueado"}), 400
        print("Buscando viajes recomendados para" + str(username))
        day = request.args.get('day')
        id_center = request.args.get('id_center')
        id_localidad = request.args.get('id_localidad')

        session = Session()
        user = session.query(User).filter_by(username=username).first()
        center = session.query(Center).filter_by(id=id_center).first()
        id_nexo = center.id_nexo
        other_centers = session.query(Center).filter_by(id_nexo=id_nexo).filter(Center.id != id_center).all()

        other_centers_ids = []
        for o_cen in other_centers:
            other_centers_ids.append(o_cen.id)

        l1 = session.query(Localidad).filter_by(id=id_localidad).first()

        ads_final=[]
        ads= session.query(Ad).filter_by(id_center=center.id).filter(Ad.id_creator != user.id)\
            .filter(Ad.day.between(day, day)).all()

        ads_one = AdSchema(many=True).dump(ads).data
        ads_final.extend(ads_one)
        ads_other = session.query(Ad).filter(Ad.id_center.in_(other_centers_ids)).filter(Ad.id_creator != user.id) \
            .filter(Ad.day.between(day, day)).all()
        ads_final.extend(AdSchema(many=True).dump(ads_other).data)

        ads_final2 = removeLocalidades(l1.latitud, l1.longitud, ads_final)

        response = jsonify(ads_final2)
        response.status_code = 200
        return response


class MyOwnTravelsSolicitedResource(Resource):
    # Solicitudes pendientes de aceptar o denegar que van en un viaje propio
    def get(self, id):
        username = request.headers.get('Authorization', default="")
        if username == "":
            return {"error": "debe especificarse el usuario logueado"}, 400
        print("Solicitando viaje de " + str(username))
        session = Session()
        user = session.query(User).filter_by(username=username).first()
        ad = session.query(Ad).filter_by(id_creator=user.id, id=id).first()
        if not ad:
            session.close()
            return {"error": "No eres propietario de ese anuncio"}, 400
        travels = session.query(Travel).filter_by(id_ad=id, creator=False, revised=False).all()
        if not travels:
            session.close()
            response = Response()
            response.status_code = 204
            return response
        travel_data= TravelSchema(many=True).dump(travels).data
        session.close()
        response = jsonify(travel_data)
        response.status_code = 200
        return response

class MyOwnTravelsTravellersResource(Resource):
    # Información de los viajeros confirmados que van en un viaje propio
    def get(self, id):
        username = request.headers.get('Authorization', default="")
        if username == "":
            return {"error": "debe especificarse el usuario logueado"}, 400
        print("Solicitando viaje de " + str(username))
        session = Session()
        user = session.query(User).filter_by(username=username).first()
        ad = session.query(Ad).filter_by(id_creator=user.id, id=id).first()
        if not ad:
            session.close()
            return {"error": "No eres propietario de ese anuncio"}, 400
        travels = session.query(Travel).filter_by(id_ad=id, creator=False, acepted=True).all()
        if not travels:
            session.close()
            response = Response()
            response.status_code = 204
            return response
        travel_data= TravelSchema(many=True).dump(travels).data
        session.close()
        response = jsonify(travel_data)
        response.status_code = 200
        return response


class MyOwnTravelsResource (Resource):
    #Permite eliminar un viaje confirmado, al creador del viaje, para el usuario indicado
    def delete(self, id, id_user):
        username = request.headers.get('Authorization', default="")
        if username == "":
            return jsonify({"error": "debe especificarse el usuario logueado"}), 400
        print("Eliminando una solicitud confirmada de un viaje de "+str(username))
        session = Session()
        user = session.query(User).filter_by(id=id_user).first()
        travel = session.query(Travel).filter_by(id_user=user.id, id_ad=id).first()
        if not travel:
            response = jsonify({"error": "no se encuentra el viaje para el usuario"})
            response.status_code = 400
            return response
        if travel.acepted == False:
            response = jsonify({"error": "Debe denegarse la solicitud, no eliminar el viaje"})
            response.status_code = 400
            return response
        session.delete(travel)
        ad = session.query(Ad).filter_by(id=id).first()
        ad.confirmed_persons = ad.confirmed_persons - 1
        session.commit()
        session.close()
        response = Response()
        response.status_code = 204
        return response
