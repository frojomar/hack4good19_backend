
import datetime
import math
import googlemaps
from itertools import tee

# API_key = 'AIzaSyB8vVm_XQDA9Nt-NOACV1PANfUczPhfbMs'#enter Google Maps API key
# gmaps = googlemaps.Client(key=API_key)


def addSecs(tm, secs):
    fulldate = datetime.datetime(100, 1, 1, tm.hour, tm.minute, tm.second)
    fulldate = fulldate + datetime.timedelta(seconds=secs)
    return fulldate.time()


def restSecs(tm, secs):
    fulldate = datetime.datetime(100, 1, 1, tm.hour, tm.minute, tm.second)
    fulldate = fulldate - datetime.timedelta(seconds=secs)
    return fulldate.time()

def formatTime(time):
    return datetime.datetime.strptime(time, '%H:%M:%S').time()



def distKmMenor(lat1, long1, lat2, long2, km):
    #TODO si distancia entre ambos puntos es menor de km, devolver true


    print("NUEVA")

    print(lat1)
    print(long1)
    print(lat2)
    print(long2)
    if lat1==lat2 and long1==long2:
        return True

    # cliente = client.Client(key='AIzaSyB8vVm_XQDA9Nt-NOACV1PANfUczPhfbMs')
    # distancia = distance_matrix.distance_matrix(cliente,[str(lat1) + " " + str(long1)],[str(lat2) + " " + str(long2)],mode='driving')['rows'][0]['elements'][0]
    #
    #
    #
    # distancia =  2 * asin(sqrt(
    #     haversin(lat2 - lat1) +
    #     cos(lat1) * cos(lat2) * haversin(long2 - long1))) * 360


    # origins = (lat1, long1)
    # destination = (lat2, long2)

    # pass origin and destination variables to distance_matrix function# output in meters
    # distancia = gmaps.distance_matrix(origins, destination, mode='walking')["rows"][0]["elements"][0]["distance"][
    #     "value"]

    # distancia = 6371.01 * acos(sin(lat1) * sin(lat2) + cos(lat1) * cos(lat2) * cos(long1 - long2))

    radius = 6371  # km

    dlat = math.radians(lat2 - lat1)
    dlon = math.radians(long2 - long1)
    a = (math.sin(dlat / 2) * math.sin(dlat / 2) +
         math.cos(math.radians(lat1)) * math.cos(math.radians(lat2)) *
         math.sin(dlon / 2) * math.sin(dlon / 2))
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))
    distancia = radius * c

    print (distancia)

    if distancia <= km:
        return True
    else:
        return False
