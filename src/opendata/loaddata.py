import json
import requests

from src.entities.base import Session, engine, Base
from src.entities.center import Center, CenterSchema
from src.entities.localidad import Localidad, LocalidadSchema


# input_file  = file("museos.geojson", "r")
# data = json.loads(input_file.read())
#
# index = 0

# museos = requests.get('http://opendata.caceres.es/sparql?default-graph-uri=&query=SELECT+%3Furi+%3Fgeo_long+%3Fgeo_lat+%3Fowl_sameAs+%3Fschema_url+%3Frdfs_label+%3Fom_tieneEnlaceSIG+%3Fschema_description+%3Fom_situadoEnVia+WHERE+%7B+%0D%0A%3Furi+a+om%3AMuseo.+%0D%0AOPTIONAL++%7B%3Furi+geo%3Along+%3Fgeo_long.+%7D%0D%0AOPTIONAL++%7B%3Furi+geo%3Alat+%3Fgeo_lat.+%7D%0D%0AOPTIONAL+%7B%3Furi+owl%3AsameAs+%3Fowl_sameAs.+%7D%0D%0AOPTIONAL++%7B%3Furi+schema%3Aurl+%3Fschema_url.+%7D%0D%0AOPTIONAL++%7B%3Furi+rdfs%3Alabel+%3Frdfs_label.+%7D%0D%0AOPTIONAL++%7B%3Furi+om%3AtieneEnlaceSIG+%3Fom_tieneEnlaceSIG.+%7D%0D%0AOPTIONAL++%7B%3Furi+schema%3Adescription+%3Fschema_description.+%7D%0D%0AOPTIONAL++%7B%3Furi+om%3AsituadoEnVia+%3Fom_situadoEnVia.+%7D%7D&format=json&timeout=0&debug=on')
# museosJSON = json.loads(museos.content)
# elements = museosJSON['results']['bindings']
#
# print(museosJSON)
# print(elements)
#
#
# europ = requests.get("http://data.europa.eu/euodp/sparqlep?query=PREFIX+dcat%3A+%3Chttp%3A%2F%2Fwww.w3.org%2Fns%2Fdcat%23%3E%0D%0APREFIX+odp%3A++%3Chttp%3A%2F%2Fdata.europa.eu%2Feuodp%2Fontologies%2Fec-odp%23%3E%0D%0APREFIX+dc%3A+%3Chttp%3A%2F%2Fpurl.org%2Fdc%2Fterms%2F%3E%0D%0APREFIX+xsd%3A+%3Chttp%3A%2F%2Fwww.w3.org%2F2001%2FXMLSchema%23%3E%0D%0APREFIX+foaf%3A+%3Chttp%3A%2F%2Fxmlns.com%2Ffoaf%2F0.1%2F%3E+select+distinct+%3Fg+%3Fo+where+%7B+graph+%3Fg+%7B%3Fs+dc%3Atitle+%3Fo.+filter+regex%28%3Fo%2C+%27Statistics%27%2C+%27i%27%29+%7D+%7D+LIMIT+10&format=json")
# europJSON = json.loads(europ.content)
# elements2 = europJSON['results']['bindings']
# print(elements2)
#
# europ = requests.get("https://services1.arcgis.com/nCKYwcSONQTkPA4K/arcgis/rest/services/ServicioSanitariosGalicia/FeatureServer/1/query?where=1%3D1&objectIds=&time=&geometry=&geometryType=esriGeometryEnvelope&inSR=&spatialRel=esriSpatialRelIntersects&resultType=none&distance=0.0&units=esriSRUnit_Meter&returnGeodetic=false&outFields=*&returnGeometry=true&multipatchOption=xyFootprint&maxAllowableOffset=&geometryPrecision=&outSR=&datumTransformation=&applyVCSProjection=false&returnIdsOnly=false&returnUniqueIdsOnly=false&returnCountOnly=false&returnExtentOnly=false&returnQueryGeometry=false&returnDistinctValues=false&orderByFields=&groupByFieldsForStatistics=&outStatistics=&having=&resultOffset=&resultRecordCount=&returnZ=false&returnM=false&returnExceededLimitFeatures=true&quantizationParameters=&sqlFormat=none&f=pjson&token=")
# europJSON = json.loads(europ.content)
# elements2 = europJSON['features']
# print(elements2)

def loadDataCenters():

    session = Session()

    centers_old = session.query(Center).all()
    for old in centers_old:
        old.active = False
    session.commit()
    session.close()

    addCenter("Hospital San Pedro de Alcantara", 39.4652, -6.36776, 1)
    addCenter("Virgen de Guadalupe", 39.473, -6.37754, 1)
    addCenter("Centro de salud del Casar de Cáceres", 39.4652, -6.36776, 2)
    addCenter("Hospital de Coria", 39.4652, -6.36776, 3)
    addCenter("Centro de Salud de Coria", 39.4652, -6.36776, 3)



def loadDataLocalidades():
    # addLocalidad("Valverde del Fresno", "1.345566", "-6.34345456")
    # addLocalidad("Moraleja", "6.34554", "-5.56654")
    # addLocalidad("Coria", "2.343443", "-4.45445")
    # addLocalidad("Casar de Cáceres", "39.560270", "-6.416548")
    # addLocalidad("Cáceres", "1.2334423", "-4.56654")

    session = Session()

    localidad_old = session.query(Localidad).all()
    for old in localidad_old:
        old.active = False
    session.commit()
    session.close()

    extrem = requests.get("https://public.opendatasoft.com/api/records/1.0/search/?dataset=espana-municipios&rows=1000&facet=communidad_autonoma&facet=provincia&facet=municipio&refine.communidad_autonoma=Extremadura")
    extremJSON = extrem.json()
    print(extremJSON)
    localidades = extremJSON['records']
    coordinates = extremJSON['records']

    for local in localidades:
        print(local)
        name= local['fields']['municipio']
        lat= local['geometry']['coordinates'][1]
        lon= local['geometry']['coordinates'][0]
        addLocalidad(name, lat, lon)

def addCenter(nombre, latitud, longitud, id_nexo):
    session = Session()

    center_find = session.query(Center).filter_by(nombre=nombre).first()
    if center_find:
        center_find.active = True
        center_find.latitud = latitud
        center_find.longitud = longitud
    else:
        center = Center(nombre, latitud, longitud, True, id_nexo)
        session.add(center)
    session.commit()
    session.close()


def addLocalidad(nombre, latitud, longitud):
    session = Session()
    loc_find = session.query(Localidad).filter_by(nombre=nombre).first()
    if not loc_find:
        localidad = Localidad(nombre, latitud, longitud)
        session.add(localidad)
    else:
        loc_find.longitud = longitud
        loc_find.latitud = latitud
    session.commit()
    session.close()
